// ==UserScript==
// @name         Improved Tradeville
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  P/E, P/BV and P/E * P/BV, Graham Numner, and Conservative Graham Number indicator
// @author       blotunga
// @match        https://2.tradeville.eu/Trading/Transaction/PersonalListNewWindow.aspx?id=-102&name=Piata%20reglementata
// @match        https://2.tradeville.eu/Trading/Transaction/PersonalListNewWindow.aspx?id=-102&name=Piata+reglementata
// @require      https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js
// @updateURL    https://bitbucket.org/sarkanyi/improved-tradeville/raw/master/index.user.js
// @downloadURL  https://bitbucket.org/sarkanyi/improved-tradeville/raw/master/index.user.js
// @grant        GM.getValue
// @grant        GM.setValue
// ==/UserScript==

const indicator_iterator = indicatorItFunc(0);
const id_prefix = 'ctl00_c_pl_PLRow_';
var num_rows = 0;
var no_shares = [];
var price = [];
var book_value = [];
var eps = [];

async function scrapePage (symbol) {
    let indicatorPage = 'https://2.tradeville.eu/Trading/ReportsAndQuotes/Indicators.aspx?symbol=' + symbol;
    let ratesPage = 'https://2.tradeville.eu/Trading/ReportsAndQuotes/FinancialRates.aspx?symbol=' + symbol;

    jQuery('#divindicator').load(indicatorPage, function() {
        let tableRow = $("td").filter(function() {
                return $(this).text() == "Numar de actiuni";
            }).closest("tr");
        if (tableRow != null && tableRow[0] != null) {
            no_shares[symbol] = parseFloat(tableRow[0].cells[1].textContent.replace(/[^\d\.]*/g, ''));
        }
        let tableRowCap = $("td").filter(function() {
                return $(this).text() == "Capitaluri proprii -total";
            }).closest("tr");
        if (tableRowCap != null && tableRowCap[0] != null) {
            let i = tableRowCap[0].cells.length - 1;
            while (tableRowCap[0].cells[i].textContent === '-') {
                i--;
            }
            book_value[symbol] = parseFloat(tableRowCap[0].cells[i].textContent.replace(/[^\d\.\-]*/g, ''))
        }
        indicator_iterator.next().value;
    });

    jQuery('#divrates').load(ratesPage, function() {
        let tableRow = $("td").filter(function() {
                return $(this).text() == "Profitul pe actiune (EPS)";
            }).closest("tr");
        if (tableRow != null && tableRow[0] != null) {
            let i = tableRow[0].cells.length - 1;
            while (tableRow[0].cells[i].textContent === '-' || tableRow[0].cells[i].textContent == '') {
                i--;
            }
            eps[symbol] = parseFloat(tableRow[0].cells[i].textContent.replace(/[^\d\.\-]*/g, ''));
        }
        indicator_iterator.next().value;
    });
    return new Promise( resolve => setTimeout(resolve.bind(this, symbol), 2000) );
}

const numberWithCommas = (x) => {
  let parts = x.toString().split(".");
  parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  return parts.join(".");
}

function updateTable(symbol, index) {
    console.log(symbol + ' ' + price[symbol] + ' ' + eps[symbol]);

    let pe = price[symbol] / eps[symbol];
    $('#'+ id_prefix + index + '_pe').text(pe.toFixed(2));
    let color = "#FFFF00"
    if (pe < 10 && pe > 0)
       color = "#00FF00";
    else if (pe > 15 || pe < 0)
        color = "#FF0000";
    $('#'+ id_prefix + index + '_pe')[0].bgColor = color;

    let bps = book_value[symbol]/no_shares[symbol];
    let pbv = price[symbol] / bps;
    $('#'+ id_prefix + index + '_pbv').text(pbv.toFixed(2));
    color = "#FFFF00"
    if (pbv < 1 && pbv > 0)
       color = "#00FF00";
    else if (pbv > 1.5 || pbv < 0)
        color = "#FF0000";
    $('#'+ id_prefix + index + '_pbv')[0].bgColor = color;

    let pepbv = pe * pbv;
    $('#'+ id_prefix + index + '_pepbv').text(pepbv.toFixed(2));
    color = "#FFFF00"
    if (pepbv < 10 && pepbv > 0)
       color = "#00FF00";
    else if (pepbv > 22.5 || pepbv < 0)
        color = "#FF0000";
    $('#'+ id_prefix + index + '_pepbv')[0].bgColor = color;

    let gn = Math.sqrt(15 * 1.5 * eps[symbol] * bps);
    $('#'+ id_prefix + index + '_gn').text(gn.toFixed(2));
    if (gn > price[symbol])
       color = "#00FF00";
    else
       color = "#FF0000";
    $('#'+ id_prefix + index + '_gn')[0].bgColor = color;

    let cgn = Math.sqrt(10 * eps[symbol] * bps);;
    $('#'+ id_prefix + index + '_cgn').text(cgn.toFixed(2));
    if (cgn > price[symbol])
       color = "#00FF00";
    else
       color = "#FF0000";
    $('#'+ id_prefix + index + '_cgn')[0].bgColor = color;

    let mgn = Math.sqrt(15 * eps[symbol] * bps);;
    $('#'+ id_prefix + index + '_mgn').text(mgn.toFixed(2));
    if (mgn > price[symbol])
       color = "#00FF00";
    else
       color = "#FF0000";
    $('#'+ id_prefix + index + '_mgn')[0].bgColor = color;

    let mv = price[symbol] * no_shares[symbol];
    $('#'+ id_prefix + index + '_mv').text(numberWithCommas(mv.toFixed(0)));
    $('#'+ id_prefix + index + '_mv')[0].align = 'right';
}

async function *indicatorItFunc(index) {
  while (index < num_rows * 2) {
      let i = Math.trunc(index / 2)
      let symbol = document.getElementById(id_prefix + i).cells[0].textContent;
      if (index % 2 == 0) {
          price[symbol] = parseFloat(document.getElementById(id_prefix + i).cells[2].textContent.replace(/[^\d\.]*/g, ''));
          $('#' + id_prefix + i).append('<td id=' + id_prefix + i + '_pe' + '></td>');
          $('#' + id_prefix + i).append('<td id=' + id_prefix + i + '_pbv' + '></td>');
          $('#' + id_prefix + i).append('<td id=' + id_prefix + i + '_pepbv' + '></td>');
          $('#' + id_prefix + i).append('<td id=' + id_prefix + i + '_gn' + '></td>');
          $('#' + id_prefix + i).append('<td id=' + id_prefix + i + '_mgn' + '></td>');
          $('#' + id_prefix + i).append('<td id=' + id_prefix + i + '_cgn' + '></td>');
          $('#' + id_prefix + i).append('<td id=' + id_prefix + i + '_mv' + '></td>');
          let x = await scrapePage(symbol);
          updateTable(symbol, i);
      }
      yield index++;
  }
}

function onReady() {
    num_rows = document.getElementById(id_prefix + '0').parentNode.rows.length - 1;
    $('body').append('<div id="divindicator" style="display: none;"></div>'); //none to hide, block to show
    $('body').append('<div id="divrates" style="display: none;"></div>'); //none to hide, block to show
    $('.header').append('<td>P/E</td>');
    $('.header').append('<td>P/BV</td>');
    $('.header').append('<td>PE*PBV</td>');
    $('.header').append('<td>GN</td>');
    $('.header').append('<td>MGN</td>');
    $('.header').append('<td>CGN</td>');
    $('.header').append('<td>Capitalizare</td>');
    indicator_iterator.next().value;
}
$(document).ready(onReady);
