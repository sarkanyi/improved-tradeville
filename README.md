# Improved Tradeville

P/E, P/BV and P/E * P/BV, Graham Numner, and Conservative Graham Number indicator
Works only on this link: https://2.tradeville.eu/Trading/Transaction/PersonalListNewWindow.aspx?id=-102&name=Piata%20reglementata
Tested to work only on Chrome and Firefox. Safari is not supported!

## How to use

Here are the steps to use: 

- Download and install the TamperMonkey addon for your browser of choice - https://tampermonkey.net/
- Open this link: https://bitbucket.org/sarkanyi/improved-tradeville/raw/master/index.user.js with TamperMonkey installed; it should open up a tab that prompts you to install the script
- And finally, open https://2.tradeville.eu/Trading/Transaction/PersonalListNewWindow.aspx?id=-102&name=Piata%20reglementata
- If only the first row is shown and GN/GNC are NaN, make sure that on the https://2.tradeville.eu/Trading/ReportsAndQuotes/Indicators.aspx?symbol=TLV page the 'Bilantul contabil' field is selected. 

## How to interpret

- P/E        - price/earnings, if < 10 green, if > 15 red, else yellow
- P/BV       - price/book_value, if < 1 green, if > 1.5 red, else yellow
- P/E * P/BV - if < 10, green if > 22.5 red, else yellow
- GN         - Graham's number (see: https://www.investopedia.com/terms/g/graham-number.asp), if share overvalued red, else green
- CGN        - Conservative approach to GN, smaller multipliers, see above.